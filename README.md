# Timber features and tricks

## Useful links

* [Timber documentation](https://timber.github.io/docs/)
* [Twig reference](http://twig.sensiolabs.org/doc/templates.html) all Twig function, operators, filters.
* [GitHub project](https://github.com/timber/timber/)
* [Debugging](https://timber.github.io/docs/guides/debugging/)
* [Cache Cleaner](https://wordpress.org/plugins/clear-cache-for-timber/)

## How install timber: 
Download or install Timber Plugin and activate.
Or install via composer. See reference. **Important!** If you wanna installing from github via composer or clone - your PHP version should be > 7.0. Set 1.x version of timber in composer.json or use plugin.

See more on [GitHub](https://github.com/timber/timber) and [WordPress plugin page](https://wordpress.org/plugins/timber-library/)

After installing - create file for Timber options, extensions, etc.
In this file you should do next steps:

**Default templates dir:**
```php
/**
* Set Timber defalut template dir
*/
Timber\Timber::$dirname = [ 'views' ];
```

**Enable cache**
```php
/**
 * Enable Cahing twig files
 */
 Timber\Timber::$cache = true;
 
 /**
 * Change Timber's cache folder.
 */
 function custom_change_twig_cache_dir() {
		  return WP_CONTENT_DIR . '/cache/timber';
 }
 add_filter( 'timber/cache/location', 'custom_change_twig_cache_dir' );
```
This is a vary important code.

Then create 'views/' in you theme folder. And place all your templates inside it.

You theme is ready to Timber development.


## A few tricks and recommendations
All global context combine into one hook.
### For usage menus you should add menus to global context in Timber.

Define menu locations: 
```php
/**
 * Register Menus.
 * Add each other menu location as array element.
 * Key - menu location slug
 * value - menu Description
 */
$menus = [
		'primary' => 'Primary Menu',
		'footer'  => 'Footer Copyright Menu',
];
foreach ( $menus as $location => $name ) {
		register_nav_menu( $location, $name );
}
```

And add to blobal context:
```php
/**
	 * Add site wide variables for using in twig files.
	 *
	 * @param array $context Array with objects or vars with global access.
	 *
	 * @return mixed
	 */
	function alutiiq_add_to_context( $context ) {

		/* Load Menus */
		foreach ( (array) get_nav_menu_locations() as $location => $id ) {
			if ( has_nav_menu( $location ) ) {
				$context[ $location ] = new Timber\Menu( $location );
			}
		}

		return $context;
	}

	add_filter( 'timber_context', 'alutiiq_add_to_context' );
```

### If you are using ACF options pages - define all fields in global context
Add this code inside `timber_context` filter function.
```php
/* Load global setting site wide */
		$context['options']       = get_fields( 'options' );
```
## Timber extensions:
### Add cstom filters:

```php
/**
	 *
	 * Add custom filters for twig templating.
	 *
	 * @param \stdClass $twig Twig class alias.
	 *
	 * @return mixed
	 */
	function alutiiq_add_filters_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );

		/**
		 * Add filters from array, where
		 * key - Twig filter name
		 * value - YOUR function name
		 */
		$filters = [
			'slice_image' => 'slice_image',
			'slice_svg'   => 'slice_svg',
			'pre'      => 'pre_callback',
		];
		foreach ( $filters as $name => $callback ) {
			$twig->addFilter( new \Twig_SimpleFilter( $name, $callback ) );
		}

		return $twig;
	}
```
And callback_function have 1 pararmeter:
```php
function pre_callback( $param ) {
	return '<pre>' . $file_name . '</pre>';
}
```

Usage in templates:
```twig
{{ post.title|pre }}
```

### Add custom functions

```php
function alutiiq_add_functions_to_twig( $twig ) {

		/**
		 * Add functions from array, where
		 * key - Twig function name
		 * value - YOUR function name
		 */
		$functions = [
			'pre'   => 'pre_callback',
		];
		foreach ( $functions as $key => $function ) {
			$twig->addFunction( new Timber\Twig_Function( $key, $function ) );
		}

		return $twig;
	}

	add_action( 'timber/twig/functions', 'alutiiq_add_functions_to_twig' );
```
Teplate usage:
```twig
{{ pre(post.title) }}
```


